import pandas as pd
import os
from pydantic import BaseModel, ValidationError
from dotenv import load_dotenv
load_dotenv()

# define constant from credential file
DATA_PATH = os.getenv("DATA_PATH")
TMP_PATH = os.getenv("TMP_PATH")
DATA_SOURCE = os.getenv("DATA_URL")
CSV_NAME = os.getenv("CSV_NAME")

# define file paths
tmp_dir = os.path.join(TMP_PATH, CSV_NAME)
data_dir = os.path.join(DATA_PATH, CSV_NAME)

# ensures necessary directories exists
if not os.path.isdir(DATA_PATH):
  os.makedirs(DATA_PATH)
if not os.path.isdir(TMP_PATH):
  os.makedirs(TMP_PATH)

class DataModel(BaseModel):
  """
  Model for validating input data using Pydantic.

  Attributes:
    CRIM (float): Crime rate by town.
    ZN (float): Proportion of residential land zoned for large lots.
    INDUS (float): Proportion of non-retail business acres per town.
    CHAS (int): Charles River dummy variable (1 if tract bounds river; 0 otherwise).
    NOX (float): Nitrogen oxide concentration (parts per 10 million).
    RM (float): Average number of rooms per dwelling.
    AGE (float): Proportion of owner-occupied units built prior to 1940.
    DIS (float): Weighted distances to five Boston employment centers.
    RAD (int): Index of accessibility to radial highways.
    TAX (int): Full-value property tax rate per $10,000.
    PTRATIO (float): Pupil-teacher ratio by town.
    LSTAT (float): Percentage of lower status of the population.
    MEDV (float): Median value of owner-occupied homes in $1000's.
  """
  CRIM: float
  ZN: float
  INDUS: float
  CHAS: int
  NOX: float
  RM: float
  AGE: float
  DIS: float
  RAD: int
  TAX: int
  PTRATIO: float
  LSTAT: float
  MEDV: float

def data_validation() -> None:
  """
  Validates the data from the temporary directory using the DataModel schema.

  Reads the data from `tmp_dir`, validates each data point, and writes valid data to `data_dir`.
  Invalid rows are removed from the dataset.
  """
  f"{' VALIDATING DATA ':=^50s}"
  # validating data
  data_to_validate = pd.read_csv(tmp_dir)
  valid_data = {}
  for idx in data_to_validate.index:
    data_point = data_to_validate.loc[idx].to_frame().T
    try:
      validated_data = DataModel(**data_point)
      print(f"data index {idx} is valid")
    except ValidationError as e:
      print(f"data index {idx} is not valid: {e}")
      data_to_validate.drop(index=idx, inplace=True)
  valid_data = data_to_validate.reset_index(drop=True)
  valid_data.to_csv(data_dir, index=False)

def fetch_realtime() -> None:
  """
  Fetches real-time data from online source and stores it in a temporary file.
  The data is read from online source saved in `DATA_SOURCE` then written 
  to `tmp_dir`.
  """
  f"{' FETCHING REALTIME DATA ':=^50s}"
  data = pd.read_csv(DATA_SOURCE)
  data.to_csv(tmp_dir, index=False)

if __name__=="__main__":
  # Fetch real-time data and validate it
  fetch_realtime()
  data_validation()