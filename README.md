# House-Price

## Data Description

Each record in the database describes a house in Boston suburb or town. The data was drawn from the Boston Standard Metropolitan Statistical Area (SMSA) in 1970. Detailed attribute information can be found below:

  - **CRIM**: Per capita crime rate by town
  - **ZN**: Proportion of residential land zoned for lots over 25,000 sq.ft.
  - **INDUS**: Proportion of non-retail business acres per town
  - **CHAS**: Charles River dummy variable (= 1 if tract bounds river; 0 otherwise)
  - **NOX**: Nitric Oxide concentration (parts per 10 million)
  - **RM**: The average number of rooms per dwelling
  - **AGE**: Proportion of owner-occupied units built before 1940
  - **DIS**: Weighted distances to five Boston employment centers
  - **RAD**: Index of accessibility to radial highways
  - **TAX**: Full-value property-tax rate per 10,000 dollars
  - **PTRATIO**: Pupil-teacher ratio by town
  - **LSTAT**: % lower status of the population
  - **MEDV**: Median value of owner-occupied homes in 1000 dollars

data source: [github.com/NishadKhudabux/Boston-House-Price-Prediction](https://github.com/NishadKhudabux/Boston-House-Price-Prediction)

## Data Processing

### Data Fetching

1. **Fetching Real-time Data:**
   - The `fetch_realtime` function retrieves data from a specified URL defined by the `DATA_SOURCE` environment variable. 
   - This data is assumed to be in CSV format.
   - The downloaded data is stored in a temporary location defined by the `TMP_PATH` environment variable.
   - The temporary file name is constructed using the `CSV_NAME` environment variable.

2. **Data Validation:**
   - The `data_validation` function performs data validation:
    - It defines a data model class (`DataModel`) using `pydantic` to define the expected schema of each data point. 
    - It reads the data from the temporary location and iterates through each row.
    - Each row is validated against the `DataModel` schema.
    - If validation is successful, a message indicating success for that data point is printed.
    - In case of validation errors, the error message is printed, and the row is removed from the data.
    - Finally, the validated data is saved to the final data directory defined by the `DATA_PATH` environment variable.

### Data Preprocessing

1. **Data Loading:**
   - The `process_data` function starts by reading the fetched & validated data from the data directory.

2. **Feature Grouping:**
   - The target variable name is defined as `"MEDV"` (from data description).
   - Feature sets are identified by separating features by data type (float and integer). 
      - Float features exclude the target variable.
      - Int features exclude the target variable and Float features.
   - A dictionary named `FEATURES` is created to store these feature sets for future reference.

3. **Data Splitting:**
   - The data is split into training and testing sets using `train_test_split` from scikit-learn.
   - The test size is set to 20% by default. 
   - A random state value (defined by the `RS` environment variable) is used to ensure reproducibility when splitting the data.

4. **Data Scaling (for float features):**
   - A `StandardScaler` object is created from scikit-learn for standardization (normalization) of the training data's float features.
   - The scaler is fitted using the training data's float features.
   - The training data's float features are then scaled using the fitted scaler.
   - The scaled features are converted back to a DataFrame with original column names and indexes.

5. **Combining Features:**
   - The scaled float features, original integer features, and target variable are concatenated back into a DataFrame for both the training and testing sets.
   - This creates the final processed data sets for training and testing.

6. **Saving Preprocessed Data:**
   - The processed training and testing sets are saved as separate CSV files in the final data directory.
   - Additionally, the feature dictionary (`FEATURES`) and the scaler object are pickled and saved for future use in the data directory.
