import pandas as pd
import os
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
import pickle
from dotenv import load_dotenv
load_dotenv()

# Define paths and data source from environment variables
DATA_PATH = os.getenv("DATA_PATH")
DATA_SOURCE = os.getenv("DATA_URL")
CSV_NAME = os.getenv("CSV_NAME")
RS = int(os.getenv("RS"))  

def process_data():
  print(f"{' START PROCESSING DATA ':=^50s}")
  # Construct the data directory path
  data_dir = os.path.join(DATA_PATH, CSV_NAME)

  # Read the data from CSV
  data = pd.read_csv(data_dir)

  # Define target variable and feature sets
  TARGET = ["MEDV"]
  FLOAT_FEATURES = [col for col in data.select_dtypes(float).columns if col not in TARGET]
  INT_FEATURES = list(set(data.columns) - set(FLOAT_FEATURES) - set(TARGET))
  FEATURES = {
      "TARGET": TARGET,
      "FLOAT_FEATURES": FLOAT_FEATURES,
      "INT_FEATURES": INT_FEATURES
  }

  # Splitting data
  train_set, test_set = train_test_split(data, test_size=0.2, random_state=RS)

  # Scaling pipeline
  scaler = StandardScaler()
  scaler.fit(train_set[FLOAT_FEATURES])

  # train set data pipeline
  scaled_train_set = scaler.transform(train_set[FLOAT_FEATURES])
  scaled_train_set_df = pd.DataFrame(scaled_train_set, columns=FLOAT_FEATURES, index=train_set.index)
  processed_train = pd.concat([scaled_train_set_df, train_set[INT_FEATURES], train_set[TARGET]], axis=1)

  # test set data pipeline
  scaled_test_set = scaler.transform(test_set[FLOAT_FEATURES])
  scaled_test_set_df = pd.DataFrame(scaled_test_set, columns=FLOAT_FEATURES, index=test_set.index)
  processed_test = pd.concat([scaled_test_set_df, test_set[INT_FEATURES], test_set[TARGET]], axis=1)

  # Save the processed training set as a CSV file
  processed_train.to_csv(
      os.path.join(DATA_PATH, "train_set.csv"),
      index=False
  )

  # Save the processed testing set as a CSV file
  processed_test.to_csv(
      os.path.join(DATA_PATH, "test_set.csv"),
      index=False
  )

  # Save the feature dictionary as a pickle file
  with open(os.path.join(DATA_PATH, "features.pkl"), 'wb') as file:
      pickle.dump(FEATURES, file)

  # Save the scaler object as a pickle file
  with open(os.path.join(DATA_PATH, "scaler.pkl"), 'wb') as file:
      pickle.dump(scaler, file)

  print(f"{' PROCESSING DATA DONE! ':=^50s}")

if __name__=="__main__":
   process_data()